// This script is for building an API for exxonmobil project with respect to surface segmentation using graph-cuts
#include "HorizonSeg2.h"
#include "LandMark.h"

using namespace std;

enum GC_technique{Vnet,VCEnet};

int main(int argc, char* argv[]){
//    if(argc < 8){
//        std::cerr << "Usage: " << std::endl;
//        std:: cerr << argv[0] << " InputImage" << " Smoothness" << " Method" <<
//                      " LandmarkSwitch" << " WindowSize" << " Outputimage" << "LayersPerPixel\n";
    if(argc < 9){
        std::cerr << "Usage: " << std::endl;
        std:: cerr << argv[0] << " InputImage" << " Smoothness" << " Method" <<
                      " LandmarkSwitch" << " WindowSize" << " LayersPerPixel" << " OutputImage" <<
                      "  bounding box factor\n";
        return EXIT_FAILURE;
    }
// Default input parameter settings: input_image 100 1 1 11 1 output_image 3

    uint D0 = 0;
    uint D1 = 1;
    uint D2 = 2;

    HorizonSegmenter HS2(argv[1], atoi(argv[6]), D0, D1, D2);


//    landmarks for Gradientimage3d_2.mhd
	// int LMarray[10][3] = { {48,6,234}, {87, 6, 234}, {109,6, 219}, {168,6,213}, {228,6,210}, {15,17,29}, {66,16,235},
	// 					   {100,16,230},{135,16,219},{213,16,212}
	// 					   /*{7,14,221}, {68,14,232}, {150,14,215}, {238,14, 203}*/}; // 2d array initialization
	// // int LMarray[10][3] = { {8,73,940}, {8,65,796}, {8,58,670},
    // //                        {34,76,709},{34,64,529},{34,34,85},
    // //                        {45,37,83}, {45,54,321}, {45,53,321}, {45,87, 768}}; // 2d array initialization
	
    // int LMarray[5][3] = { {8,11,28}, {8,22,42}, {8,14,32},
    //                       {3,22,37},{3,10,22} }; // 2d array initialization
// landmarks for s2_cf.nrrd
//    int LMarray[15][3] = {{39,52,175},{39,76,82},{39,92,20},
//                      {53,43,213},{53,61,135},{53,80,62},
//                      {67,47,199},{67,66,119},{67,89,28},
//                      {118,37,238},{118,73,93},{118,89,31},
//                      {149,46,199},{149,62,136},{149,83,53}};
// landmarks for s2_cf_cropped.nrrd
//    int LMarray[15][3] = {{17,66,20},{17,62,36},{17,57,56},
//                         {40,67,17},{40,62,36},{40,58,52},
//                         {72,67,16},{72,63,33},{72,58,52},
//                         {94,70,5},{94,64,30},{94,58,53},
//                         {118,66,20},{118,63,32},{118,56,60}};

    int LMarray[6][3] = {{56 ,6 ,36}, {59 ,22 ,37}, {185 ,6 ,16}, {201 ,22 ,16}, {    240 ,6 ,6} , {    241 ,22 ,4}};
    uint NoLandmarks = sizeof(LMarray)/sizeof(LMarray[0]); // sizeof(LMarray)/sizeof(*LMarray)
    std::cout << "Number of landmarks: " << NoLandmarks << "\n";
    for(uint m = 0; m < NoLandmarks; m++){
        point p;
        p[0] = LMarray[m][D0];
        p[1] = LMarray[m][D1];
        p[2] = LMarray[m][D2];
        HS2.AddLandmark(p);
    }

    HS2.SetSmoothnessConstraint(atoi(argv[2])); // setting smoothness constraint
    HS2.SetGraphCutMethod(atoi(argv[3])); // setting Graph-cut method
    HS2.SetSwitchToPassThroughLandmark(atoi(argv[4])); // radio switch to pass through landmark
    HS2.SetProfileLength(atoi(argv[5])); // setting window size
    HS2.SetOutputImageName(argv[7]); // setting output image name
    HS2.SetNarrowBandFactor(atoi(argv[8])); // bounding box factor

//    // calling wrapper
    HS2.Solver();
    HS2.WriteOutputImage();
//    vec3i SegRegion = HS2.GetRegion();
//    vec2f SegHorizon = HS2.GetBoundary();
}



