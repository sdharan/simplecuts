#ifndef DEFS_H
#define DEFS_H

//#define NDEBUG // comment for testing.
#define DEF_NPROC 20
#define uint unsigned  int
#define G_COUNTER_UPDATE_FREQ 1
#define GR_FACTOR 0.1
#define GR_SLEEP_TIME 1
#define DO_GLOBAL_RELABEL true


//#define GETNODEGRQPREV (v,i) (i == 0 ? v->_1_gq_prev : v->_2_gq_prev)
//#define GETNODEGRQNEXT (v,i) (i == 0 ? v->_1_gq_next : v->_2_gq_next)

//#define RELABEL_TO_END 1
#define HI_PR 1
// Timer functionality
#include <sys/time.h>


/////// Improvements ?
// Maybe always have the shared and lockable vertices in the queue,
// This way, you can avoid the queue locks.

// Choose a way to alter the shared and lockable vertices midway
// and check if it makes a differece. This would work by decreasing
// the size of the first segment and increasing the size of the last
// segmenet and then all the middle segments have the same number of columns,
// but just shifted towards the left a little.



#endif // DEFS_H
