#include <iostream>
#include "graph.h"
#include <vector>
#include <string>
#include <vtk/vtkMetaImageReader.h>
#include <vtk/vtkImageData.h>
#include <vtk/vtkSmartPointer.h>
#include <sstream>
#include <fstream>
#include <math.h>
#include <boost/thread/thread_only.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/thread/detail/thread_group.hpp>
#include "block.h"

#define INFVAL 1E+10
using namespace std;

//#define sub2ind(x,y) ((x*60)+y)
#define ALPHA 100
#define BETA 2
#define ARCWINDOW 5
float DELTASL(unsigned int f, int alpha, int beta){ // SL = same level
  float fa = alpha * (pow((float)(f+1),beta));
    float sl = alpha * (pow((float)f,beta));
    return((fa - sl)); // cost penalty edge function
}

float DELTALB(unsigned int f, int alpha, int beta){ // LB == level below
    float fa = alpha * (pow((float)(f+1),beta));
    float sl = alpha * (pow((float)f,beta));
    float fb = alpha * (pow((float)(f-1),beta));
    return((fa-sl) - (sl-fb)); // cost penalty edge function
}
void edge_cost(float* delta_ij){
    for(int m = 0; m < ARCWINDOW; m++){
        if(m == 0){
            float deltaij0 = DELTASL(m, ALPHA, BETA);
            delta_ij[m] = deltaij0;
        }else{
            float deltaijxplus1 = DELTALB(m, ALPHA, BETA);
            delta_ij[m] = deltaijxplus1;
        }
    }
}
int getEdgeCount(int r, int c) {
    return (r * c
            + (r-1) * c
            + (r-1) * 2*ARCWINDOW * (c-2)
            + (r-1) * 2*ARCWINDOW * 2
            + c);
}
void readAndConvertMHDFile(){
    std::string mhd_filename = "/home/shri/network/mincut/data/GradientImage2d.mhd";
    int imageDims[3];

    vtkSmartPointer<vtkMetaImageReader> reader =
            vtkSmartPointer<vtkMetaImageReader>::New();
    reader->SetFileName(mhd_filename.c_str());
    reader->Update();
    vtkImageData *imData = reader->GetOutput();
    imData->GetDimensions(imageDims);


    int nRows = imageDims[0];
    int nCols = imageDims[1];


    ofstream iif(mhd_filename.append("_ascii").c_str());

    float m_pixelvals[nRows][nCols];
    for (int i = 0; i < nRows; i++) {
        for (int j = 0; j < nCols; j++) {
            iif << imData->GetScalarComponentAsFloat(i,j,0,0) << " ";
            //std::cout << m_pixelvals[i][j] << " ";
        }
        iif << std::endl;
        //std::cout << std::endl;
    }

}

int main () {
    std::string mhd_filename = "/home/shri/network/mincut/data/out1.mat";

//    mhd_filename.append(".mhd");

//    int imageDims[3];

//    vtkSmartPointer<vtkMetaImageReader> reader =
//            vtkSmartPointer<vtkMetaImageReader>::New();
//    reader->SetFileName(mhd_filename.c_str());
//    reader->Update();
//    vtkImageData *imData = reader->GetOutput();
//    imData->GetDimensions(imageDims);


//    int nRows = imageDims[0];
//    int nCols = imageDims[1];

    //readAndConvertMHDFile();
    int nRows = 128;
    int nCols = 1024;
    ifstream iif(mhd_filename.c_str());

    float m_pixelvals[nRows][nCols];
    for (int i = 0; i < nRows; i++) {
        for (int j = 0; j < nCols; j++) {
            iif >> m_pixelvals[i][j];
            //std::cout << m_pixelvals[i][j] << " ";
        }
        //std::cout << std::endl;
    }

    int num_segments = 1;

    shared_ptr<Graph<float,float,float> > g(new Graph<float,float,float>(nRows*nCols, getEdgeCount(nRows,nCols),num_segments,nRows,nCols));
    for (int i = 0; i < nRows*nCols; i++) {
        g->add_node();
    }

    float m_weights[nRows][nCols];
    for(int m = 0; m < nRows; m++){
        for(int n = 0; n < nCols; n++){
            if(m < (nRows-1)){
                m_weights[m][n] = m_pixelvals[m][n] - m_pixelvals[m+1][n];
            }else{
                m_weights[m][n] = m_pixelvals[m][n];
            }
        }
    }

    float Delta_ij[nRows*nCols];
    edge_cost(Delta_ij);

    for(int m = 0; m < nRows; m++){
        for(int n = 0; n < nCols; n++){
            // T-links
            float TlnkCost = m_weights[m][n];
            //std::cout << TlnkCost << " ";
            if(TlnkCost < 0)
                g->add_tweights((m*nCols)+n, -TlnkCost,0);
            else
                g->add_tweights((m*nCols)+n, 0, TlnkCost);
            // N-links
            // arc from a node to the node below it
            if(m < (nRows-1)){
                g->add_edge((m*nCols)+n,((m+1)*nCols)+n,INFVAL,0);
            }else if(m == (nRows-1)){ // base graph
                if(n == 0 || (n < (nCols-1) && ((n+1) % (nCols/num_segments)) != 0)) {
                    g->add_edge((m*nCols)+n,((m*nCols)+(n+1)),INFVAL,INFVAL);
                }
                else if (n+1 < nCols) {
                    //std::cout << "Skipping arc from " << n << "to " << n+1 << std::endl;
                }
                //g->add_edge((m*nCols)+n,((m*nCols)+(n+1)),0,0);

            }
        }
    }
    for(int m = 0; m < (nRows-1); m++){
        for(int n = 0; n < nCols; n++){
            if(n == 0){
                for(int fij = 0; fij < (nRows - m); fij++){
                    if(fij < ARCWINDOW){
                        g->add_edge((m*nCols)+n,((m+fij)*nCols)+(n+1),Delta_ij[fij], 0);
                    }
                }
            }else if(n == (nCols-1)){
                for(int fij = 0; fij < (nRows - m); fij++){
                    if(fij < ARCWINDOW){
                        g->add_edge((m*nCols)+n,((m+fij)*nCols)+(n-1),Delta_ij[fij], 0);
                    }
                }
            }else{
                int cols_per_segment = nCols/ num_segments;
                for(int fij = 0; fij < (nRows - m); fij++){
                    if(fij < ARCWINDOW){
                        if ((n+1 == nCols-1) || floor(n/cols_per_segment) == floor((n+1)/cols_per_segment)) {
                            g->add_edge((m*nCols)+n,((m+fij)*nCols)+(n+1),Delta_ij[fij], 0);
                        } else {
                            //g->add_edge((m*nCols)+n,((m+fij)*nCols)+(n+1),0, 0);
                           // std::cout << "skipping arc from " << n << " to " << n+1 <<  " becau" << std::endl;
                        }
                        if ((n-1 == 0) || floor(n/cols_per_segment) == floor((n-1)/cols_per_segment)) {
                            g->add_edge((m*nCols)+n,((m+fij)*nCols)+(n-1),Delta_ij[fij], 0);
                        }
                        else {
                            //g->add_edge((m*nCols)+n,((m+fij)*nCols)+(n+1),0, 0);
                            //std::cout << "skipping arc from " << n << " to " << n-1 << std::endl;
                        }
                    }
                }
            }
        }
    }

    g->analyze_stuff();

    typedef Graph<float,float,float> GraphType;
    boost::thread_group tg;

    boost::thread *t = new boost::thread(boost::bind( &GraphType::maxflow, g, 0, false, (Block<int>*) 0) );//fun(i, false, (Block<int>* )NULL));
    t->join();

    for (int i = 1; i < num_segments; i++) {
        //boost::function<void (int, bool,  Block<int>*) > fun = boost::bind( &GraphType::maxflow, g, _1, _2, _3);
        //std::cout << "Starting thread " << i << std::flush;
        boost::thread *t = new boost::thread(boost::bind( &GraphType::maxflow, g, i, false, (Block<int>*) 0) );//fun(i, false, (Block<int>* )NULL));
        tg.add_thread(t);
        //t->join();
    }

    tg.join_all();
//    std::cout << " \n \n \n \n \n \n\n\n\n\n";
//    g->analyze_stuff();
//    for(int m = 0; m < nRows; m++){
//        for(int n = 0; n < 100; n++){
//            if(m == (nRows-1)){ // base graph
//                if(n == 0 || (n < (nCols-1) && ((n+1) % (nCols/num_segments)) != 0) || n == nCols-1) {
//                    //std::cout << "Skipping arc from " << n << "to " << n+1 << std::endl;
//                }
//                else {
//                    g->add_edge((m*nCols)+n,((m*nCols)+(n+1)),INFVAL,INFVAL);
//                    g->mark_node((m*nCols)+n, 0);
//                    g->mark_node(((m*nCols)+(n+1)), 0);
//                }

//            }
//        }
//    }

//    for(int m = 0; m < (nRows-1); m++){
//        for(int n = 0; n < 100; n++){
//            int cols_per_segment = nCols/ num_segments;
//            for(int fij = 0; fij < (nRows - m); fij++){
//                if(fij < ARCWINDOW){
//                    if ((n==nCols -1) || (n+1 == nCols-1) || floor(n/cols_per_segment) == floor((n+1)/cols_per_segment)) {
//                        //std::cout << "skipping arc from " << n << " to " << n+1 <<  " becau" << std::endl;
//                    } else {
//                        g->add_edge((m*nCols)+n,((m+fij)*nCols)+(n+1),Delta_ij[fij], 0);
//                        g->mark_node((m*nCols)+n, 0);
//                        g->mark_node(((m+fij)*nCols)+(n+1), 0);
//                    }
//                    if ((n-1 == 0) || floor(n/cols_per_segment) == floor((n-1)/cols_per_segment)) {
//                        std::cout << "skipping arc from " << n << " to " << n-1 << std::endl;
//                    }
//                    else {
//                        g->add_edge((m*nCols)+n,((m+fij)*nCols)+(n-1),Delta_ij[fij], 0);
//                        g->mark_node((m*nCols)+n, 0);
//                        g->mark_node(((m+fij)*nCols)+(n-1),0 );
//                    }
//                }
//            }
//        }
//    }

//    g->maxflow(true, NULL, 0);

    int gc[nRows][nCols];
    for (int i = 0; i < nRows; i++) {
        for (int j = 0; j < nCols; j++) {
            gc[i][j] = 0;
        }
    }
    for(int n = 0; n < nCols; n++){
        for(int m = 0; m < nRows; m++){
            if(((m > 0) && ((g-> what_segment(((m-1)*nCols) + n) == Graph<float,float,float>::SINK) &&
                            (g-> what_segment((m*nCols) + n) == Graph<float,float,float>::SOURCE) ) ) ||
                    ((m == 0) && (g-> what_segment((m*nCols) + n) == Graph<float,float,float>::SOURCE) ) ){
                for(int mm = m; mm < nRows; mm++)
                    gc[mm][n] = 1;
            }
        }
    }

    std::ostringstream s;
    s << mhd_filename << "_" << num_segments << "_" << ALPHA << ".pbm";
    ofstream f(s.str().c_str());
    std::cout << "Writing to file" << s.str();

    f << "P1" << std::endl;
    f << nCols << " " << nRows << std::endl;

    for (int i = 0; i < nRows; i++) {
        for (int j = 0; j < nCols; j++ ) {
            f << gc[i][j] << " ";
            //std::cout << gc[i][j];
        }
        f << std::endl;
        //std::cout << std::endl;
    }

    f.close();
    // Zero out boundary edges.
    return 0;
}
