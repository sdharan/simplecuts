#include "ds.h"
#include <unistd.h>

Node::Node(): sink(false), source(false), lockable(false), shared(false), wavenumber(0),q_next(NULL), q_prev(NULL),gq_next(NULL), gq_prev(NULL) {
    edge_list = NULL;
    label = 0;
    st = true;
    excess = 0;
    inQueue = false;
    rank = WHITE;
    inGlobalQueue = false;
}

bool Node::isSink() {
    return sink;
}

bool Node::isSource() {
    return source;
}

Node *Node::setLockable(bool val) {
    this->lockable = val;
    return this;
}

Node *Node::setShared(bool val) {
    this->shared = val;
    return this;
}

bool Node::isLockable() {
    return lockable;
}

bool Node::isShared() {
    return shared;
}

void Node::add_edge(Edge *e) {
    //assert(!buildComplete && "Edit Node after build stage");

    e->next = NULL;
    if (edge_list == NULL){
        edge_list = e;
        return;
    } else {
        e->next = edge_list;
        edge_list = e;
    }
    return;
}

//void Node::setLocation(int r, int c, int s) {
//    this->r = r;
//    this->c = c;
//    this->s = s;
//}

void Graph::relabel(Node *v, int pid) {
    Edge *edge_list = v->edge_list;
    size_t poslabel = numNodes;
    bool set= false;
    //    if (v->isLockable()) {
    //        v->lock.lock();
    //    }
    if (v->excess == 0) {
        return ;
    }

    Edge *rank_list = NULL;
    while (edge_list) {
        if (edge_list->getCap() > 0) {
            if (poslabel > edge_list->getNode()->label) {
                poslabel =   edge_list->getNode()->label;
                rank_list = edge_list;
                set = true;
            }
        }
        edge_list = edge_list->next;
    }
    if (!set) {
        v->label = numNodes + 1;
    } else if (poslabel >= v->label) {
        v->label = poslabel + 1;
    }
    if (v->label < numNodes) {
        v->current_edge = rank_list;
    }

}

status Graph::apply(Node *u, int pid) {

    //    if (u->isLockable()) {
    //        u->lock.lock();
    //    }

    if (u->label >= numNodes || u->excess == 0) {
        if (u->isLockable()){
            u->lock.unlock();
        }
        return END;
    }

    //std::cout << u->inQueue << " " << u->label<<std::flush << std::endl;
    //int apid = (u->c * num_procs) / columns;
    assert(u->label <=  numNodes);

    Edge *edge = u->current_edge;
    while(edge && u->excess > 0) {
        if(edge->getCap() > 0) {
            Node *v = edge->getNode();
            if (v->isLockable())  {
                // Get lock
                if (!v->lock.try_lock()){
                    // Release all locks and return as WAIT
                    if (u->excess > 0 && u->label < numNodes) {
                        //q->push(u,pid);
                        addToQueue(u, false, pid);
                    }

                    return WAIT;
                }
            }
            // Locked all the required objects
            if (v->label == u->label - 1 && (v->isSink() || v->wavenumber == u->wavenumber)) {
                int del = std::min(u->excess, edge->getCap());
                assert(del > 0);
                edge->increaseFlow(del);
                u->excess = u->excess - del;
                v->excess = v->excess + del;

                //                    if (v->isSink()) {
                //                        std::cerr << "Increased flow to sink by " << del << " to " << v->excess << std::endl << std::flush;
                //                    }
                // If v has become active, then add it to the queue;

                if (v->excess > 0 && v->label < numNodes  && !v->source && !v->sink){
                    addToQueue(v, false,pid); // adds to queue if its not already in the queue.
                    //v->current_edge = v->edge_list;
                }
            }

            if (v->isLockable()) {
                v->lock.unlock();
            }
        }
        edge = u->current_edge = u->current_edge->next;
        //pushcount++;
    }
    relabel(u,pid);
    //std::cerr << "Node id : " << u->id<< " label :" << u->label << " excess " << u->excess << std::endl << std::flush;
    if ( u->label < numNodes && u->excess > 0) {
        addToQueue(u,false,pid);
    }

    return END;
}

// If v is to be locked, it has to be locked by the calling function
void Graph::addToQueue(Node *v, bool getLock, int pid) {
    if (!v->inQueue) {
        //int qid = this->getPID(v);
        int qid = v->getPID();
        boost::mutex::scoped_lock _scoped_lock(this->m_tsdata[qid].queue_mutex);

        if (this->m_tsdata[qid].queue_counter == 0) {
            m_done_flags[qid] = false;
        }
        this->m_tsdata[qid].queue_counter++;

        assert (v->q_next == NULL && v->q_prev == NULL);
        assert(getLock || v->isLockable() || qid == pid);
        assert(!v->inQueue);
        //q->push(v, qid);
        if (m_tsdata[qid].header == NULL){
            // Queue is empty.
            m_tsdata[qid].header = m_tsdata[qid].tail = v;
            //v->q_next = v->q_prev = NULL; // its already null
        }
        else {
            if (v == m_tsdata[qid].header) {
                std::cerr << "Bad queue" << std::endl << std::flush;
                exit(0);
            }
            v->q_next = m_tsdata[qid].header;
            m_tsdata[qid].header->q_prev = v;
            m_tsdata[qid].header = v;
        }
        v->inQueue = true;
    }
}


size_t Graph::getFlowValue() {
    size_t flow = 0;
    for (size_t i = 0; i < num_procs; ++i) {
        flow += this->m_tsdata[i].sink.excess;
    }
    return flow;
}

void Graph::add_tweights(unsigned int id, float cap_source, float cap_sink) {
    //flow += (cap_source < cap_sink) ? cap_source : cap_sink;
    //nodes[id].tr_cap = cap_source - cap_sink;
    assert((cap_source >= 0 && cap_sink >= 0));

    float tcapacity = cap_source - cap_sink;

    Edge *terminal_to_node = this->create_new_edge();
    Edge *node_to_terminal = this->create_new_edge();
    terminal_to_node->setMate(node_to_terminal);
    node_to_terminal->setMate(terminal_to_node);
    //int pid = this->getPID(&nodes[id]);
    int pid = nodes[id].getPID();
    if (tcapacity > 0) {
        // connected to the source;
        node_to_terminal->setNode(&this->m_tsdata[pid].source);
        terminal_to_node->setNode(&nodes[id]);
        nodes[id].add_edge(node_to_terminal);
        this->m_tsdata[pid].source.add_edge(terminal_to_node);
        terminal_to_node->setCap(tcapacity);

    } else {
        node_to_terminal->setNode(&this->m_tsdata[pid].sink);
        terminal_to_node->setNode(&nodes[id]);
        nodes[id].add_edge(node_to_terminal);
        this->m_tsdata[pid].sink.add_edge(terminal_to_node);
        node_to_terminal->setCap(-tcapacity);
    }
}

Graph::Graph(size_t num_edges, size_t num_nodes, size_t rows, size_t cols, size_t slices, size_t num_procs)
    : numEdges(num_edges),
      numNodes(num_nodes),
      num_procs(num_procs),
      rows(rows),
      columns(cols),
      slices(slices),
      m_wavenumber(0),
      m_currentLevel(0),
      //q(NULL),
      m_dischargecounter(0)
      //m_brelabel(NULL)
{

    std::cout << sizeof(ThreadStructures) << " " << sizeof(Edge) << " " << sizeof(Node);
    std::cout << num_nodes << " " << num_edges << std::endl;
//    sink = new Node[num_procs];
//    source = new Node[num_procs];
    // Make sure the nodes are not in the same cache line // TODO
//    for (size_t i = 0; i < num_procs; ++i){
//        sink[i].sink = true;
//        sink[i].source = false;
//        source[i].source = true;
//        source[i].sink  = false;
//        source[i].lockable = source[i].shared = sink[i].lockable = sink[i].shared = true;
//        source[i].label = numNodes + 1;
//    }

    so = SizingOracle(rows, columns, slices);
    edges = new Edge[num_edges];
    nodes = new Node[num_nodes];

    cols_per_segment = columns/num_procs;
    for (size_t k = 0; k < slices; k++) {
        for (size_t i = 0; i < rows; ++i) {
            for (size_t j = 0; j < cols; j++) {
                size_t index = so.sub2ind(i,j,k);
                //nodes[index].setLocation(i,j,k);
                nodes[index].setPID(min(j / cols_per_segment, num_procs-1));

                if (j > 0 && j < columns - 1 && j%(columns/num_procs) == 0) {
                    nodes[index].setLockable(true);
                    nodes[index].setShared(true);
                    nodes[so.sub2ind(i,j-1,k)].setLockable(true);
                    nodes[so.sub2ind(i,j+1,k)].setLockable(true);
                }
            }
        }
    }

    last_array_edge = edges;
    //m_brelabel = new bool[num_procs]();
    //    for (size_t i = 0; i < num_procs; ++i) {
    //        m_brelabel[i] = !DO_GLOBAL_RELABEL;
    //    }

    //    m_wait_conditions = new boost::condition_variable[num_procs];
    //    m_wait_mutexes = new boost::mutex[num_procs];

    //m_label_counter_mutex = new boost::mutex[numNodes];
    //label_counter.resize(numNodes,0);
    m_done_flags = new boost::atomic<bool>[num_procs]();

    for (size_t i = 0; i < num_procs; ++i) {
        m_done_flags[i] = false;
    }
    // Queue implementation.

    m_tsdata = new ThreadStructures[num_procs]();
    for (size_t i = 0; i < num_procs; ++i) {
        m_tsdata[i].source.label = numNodes+1;
    }

    ids = new boost::thread::id[num_procs];

}

double my_diff_ms(timespec t1, timespec t2)
{
    return (((t1.tv_sec - t2.tv_sec) * 1000000) +
            (t1.tv_nsec - t2.tv_nsec))/1000000;
}

size_t Graph::maxflow() {

    time_t start;
    time(&start);
    this->initialize_preflow();
    // create threads.

    boost::thread_group tg;
    for (size_t i = 0; i < num_procs; ++i) {
        tg.create_thread(boost::bind(&Graph::discharge, this, i));
    }
    tg.create_thread(boost::bind(&Graph::search_allocator, this));

    tg.join_all();
    time_t end;
    time(&end);
    std::cout << "timetaken =  " << difftime(start,end) << std::endl << std::flush;

//    for (size_t i = 0; i < numNodes; ++i){
//        nodes[i].label = numNodes+1;
//        nodes[i].rank = WHITE;
//        nodes[i].st = true;
//    }

    std::queue <Node*> localq;
    for (size_t i = 0;i < num_procs; ++i){
        //Edge *e = sink[i].edge_list;
        Edge *e = this->m_tsdata[i].sink.edge_list;
        while (e){
            if (e->getMate()->getCap() > 0 && e->getNode()->rank == WHITE) {
                localq.push(e->getNode());
                e->getNode()->rank = GREY;
                e->getNode()->st = false;

            }
            e = e->next;
        }
    }
    while (!localq.empty()){
        Node *n = localq.front();
        localq.pop();
        Edge *e = n->edge_list;
        while(e){
            if (e->getMate()->getCap() > 0 && e->getNode()->rank == WHITE && !e->getNode()->isSink() && !e->getNode()->isSource()) {
                localq.push(e->getNode());
                e->getNode()->rank = GREY;
                e->getNode()->st = false;
            }
            e = e->next;

        }
    }
    return this->getFlowValue();
}
//void Graph::setST(int pid) {
//    Edge *e = sink[pid].edge_list;

//}

void Graph::dump_segmentation(std::string filename) {
    std::cerr << "DUMPING FILE  ****************" << filename << std::endl;
    ofstream out(filename.c_str());
    for (size_t i = 0; i < numNodes; ++i) {

        if (nodes[i].st == 0) {
            out << 1 << " ";
        } else {
            out << 0 << " ";
        }
    }
}

//void Graph::global_relabel(Node *v, size_t pid) {
//    // Simply add its neighbors after changing its label.
//    if (v->isLockable())  {
//        v->lock.lock();
//    }
//    assert(v->wavenumber < m_wavenumber);
//    v->wavenumber = m_wavenumber;
//    v->label = m_currentLevel;

//    Edge *iter_edge = v->edge_list;
//    while (iter_edge) {
//        Node *u = iter_edge->getNode();
//        if (u->isSink() || u->isSource()){
//            return;
//        }
//        // u->wavenumber
//        if (iter_edge->getCap() > 0 && u->wavenumber < m_wavenumber) {
//            u->wavenumber = m_wavenumber;
//            u->label = v->label+1;
//            // Add u to the bfs queue.
//            // TODO
//            gr_q->push(u,pid,GRELABEL);
//        }
//        if (u->isLockable()) {
//            u->lock.unlock();
//        }
//        iter_edge = iter_edge->next;
//    }

//    if (v->isLockable()) {
//        v->lock.unlock();
//    }
//}

//std::queue<Node*> Graph::getRelabelQueue(int pid) {
//    //boost::shared_lock<boost::shared_mutex> _lock(m_lsgr_mutex);

//    int queueid = m_currentLevel %2;
//    //std::queue<Node* this->gr_q[queueid]->getQueue(pid);

//}

void Graph::updateLabelCounter(size_t oldValue, size_t newValue) {
    if (oldValue > 0) {
        boost::mutex::scoped_lock lock(m_label_counter_mutex[oldValue]);
        this->label_counter[oldValue]--;
        if (this->label_counter[oldValue] == 0) {
            boost::mutex::scoped_lock _gap_lock(m_gap_mutex);
            if (!GapFound || GapLevel > oldValue) {
                GapFound = true;
                GapLevel = oldValue;
            }
        }
    }
    if  (newValue < numNodes){
        boost::mutex::scoped_lock lock(m_label_counter_mutex[newValue]);
        this->label_counter[newValue]++;
    }
}



void Graph::global_relabel(size_t pid) {
    if (m_currentLevel == 1 || m_currentLevel == numNodes+1) {
        // The localq queue will be empty, just get the lock on sink and add all the edges
        // leading to nodes in this segment
        Node *s = m_currentLevel == 1 ? &this->m_tsdata[pid].sink : &this->m_tsdata[pid].source;
        s->lock.lock();
        Edge *e = s->edge_list;
        while (e) {
            if (s->isSource() || e->getMate()->getCap() > 0) {
                Node *u = e->getNode();
                if (u->isLockable()) {
                    u->lock.lock();
                }
                // Get a lock on this node. There cannot be a deadlock here,
                // because u has to be processed by the same processor.
                if (u->wavenumber == m_wavenumber){
                    e = e->next;
                    if (u->isLockable()) {
                        u->lock.unlock();
                    }
                    continue;
                }
                assert(u->wavenumber < m_wavenumber);

                assert(u->getPID() == pid);
                u->wavenumber = m_wavenumber;
                if (u->label < m_currentLevel) {
                    u->label = m_currentLevel;
                }

                this->addToGlobalQueue(u,pid, (m_currentLevel+1)%2);

                u->relabel_scan_list = u->edge_list;

                // if this node  becomes active, add it to the queue.
                if (!u->inQueue && u->excess > 0 && u->label < numNodes){
                    assert(false);
                    addToQueue(u,false, pid);
                }

                if (u->isLockable()) {
                    u->lock.unlock();
                }
            }
            e = e->next;
        }

        s->lock.unlock();
        return;
    }

    // Get values from the queue and process them.
    Node *v;
    while (getFromGlobalQueue(v, pid,m_currentLevel%2)) {
        assert (v->wavenumber == m_wavenumber);

        Edge *e = v->relabel_scan_list;

        while (e) {
            Node *u = e->getNode();
            if (!(u->isSink() || u->isSource())) {

                if (u->isLockable()) {
                    if (!u->lock.try_lock()) {
                        this->addToGlobalQueue(v, pid, m_currentLevel%2);
//                        if (v->isLockable()) {
//                            v->lock.unlock();
//                        }
                        //localq.push(v);
                        break;
                    }
                }
                // all elements are locked here.

                if ((m_currentLevel > numNodes || e->getMate()->getCap() > 0) && u->wavenumber < m_wavenumber) {
                    //gr_q[(m_currentLevel+1)%2]->push(u,this->getPID(u));
                    this->addToGlobalQueue(u,u->getPID(), (m_currentLevel+1)%2);

                    u->relabel_scan_list = u->edge_list;
                    u->wavenumber = m_wavenumber;
                    if (u->label < m_currentLevel) {
                        u->label= m_currentLevel;
                    }
                }

                if (u->excess > 0 && u->label < numNodes){
                    if (!u->inQueue) {
                        std::cerr << "heer" << std::flush;
                    }
                    addToQueue(u,false, pid);
                }

                if (u->isLockable()) {
                    u->lock.unlock();
                }

            }
            e = v->relabel_scan_list = e->next;

        }

        if (v->isLockable()) {
            v->lock.unlock();
        }
    }
}

bool Graph::getFromQueue(Node *&v, int pid) {
    boost::mutex::scoped_lock _scoped_lock(m_tsdata[pid].queue_mutex);
    if (m_tsdata[pid].tail == NULL){
        return false;
    }
    // Queue is not empty.
    if (m_tsdata[pid].tail == m_tsdata[pid].header) {
        v = m_tsdata[pid].tail;
        assert(v->q_next == NULL && v->q_prev == NULL);
        m_tsdata[pid].tail = m_tsdata[pid].header = NULL;
        if (v->isLockable()) {
            v->lock.lock();
        }
        v->inQueue = false;
        return true;
    }
    // Queue has more than one element in it.
    v = m_tsdata[pid].tail;
    if (v->isLockable()) {
        v->lock.lock();
    }

    assert(v->q_next == NULL);
    assert(v->q_prev != NULL);

    m_tsdata[pid].tail = v->q_prev;
    m_tsdata[pid].tail->q_next = NULL;
    v->q_prev = NULL;

    v->inQueue = false;
    return true;
}


////// If v is to be locked, it has to be locked by the calling function
void Graph::addToGlobalQueue(Node *v, int pid, int evenodd) {
    //if (!(GETNODEGRQPREV(v,evenodd) || GETNODEGRQNEXT(v,evenodd))) {
    assert(evenodd == 0 || evenodd == 1);
        //int qid = this->getPID(v);
    if (!v->inGlobalQueue) {
        int qid = v->getPID();
        boost::mutex::scoped_lock _scoped_lock(this->m_tsdata[qid].gobal_queue_mutex);

        this->m_tsdata[qid].global_queue_counter[evenodd]++;
        assert (v->gq_next == NULL && v->gq_prev == NULL);
        assert(!v->inGlobalQueue);

        if (this->m_tsdata[qid].queueheader[evenodd] == NULL){
            // Queue is empty.
            assert(this->m_tsdata[qid].global_queue_counter[evenodd] == 1);
            m_tsdata[qid].queueheader[evenodd] = m_tsdata[qid].queuetail[evenodd] = v;
        }
        else {
            if (v == m_tsdata[qid].queueheader[evenodd]){
                assert(false);
            } else {
                v->gq_next = m_tsdata[qid].queueheader[evenodd];
                m_tsdata[qid].queueheader[evenodd]->gq_prev = v;
                m_tsdata[qid].queueheader[evenodd] = v;
            }
        }
        v->inGlobalQueue = true;
    } else if(!(v->isLockable())) {
        std::cerr << "I came here!";
    }
}

bool Graph::isGlobalQueueEmpty(int evenodd){
    assert(evenodd == 0 || evenodd == 1);
    for (size_t i = 0; i < num_procs; ++i) {
        if (m_tsdata[i].global_queue_counter[evenodd] != 0) {
            return false;
        }
    }

    return true;
}

bool Graph::getFromGlobalQueue(Node *&v, int pid, int evenodd) {
    boost::mutex::scoped_lock _scoped_lock(this->m_tsdata[pid].gobal_queue_mutex);
    assert(evenodd == 0 || evenodd == 1);

    if (this->m_tsdata[pid].queuetail[evenodd] == NULL){
        assert(m_tsdata[pid].global_queue_counter[evenodd] == 0);
        return false;

    }
    m_tsdata[pid].global_queue_counter[evenodd]--;
    // Queue is not empty.
    if (this->m_tsdata[pid].queuetail[evenodd] == this->m_tsdata[pid].queueheader[evenodd]) {
        v = this->m_tsdata[pid].queuetail[evenodd];
        assert(m_tsdata[pid].global_queue_counter[evenodd] == 0);
        assert(v->gq_next== NULL && v->gq_prev == NULL);
        this->m_tsdata[pid].queuetail[evenodd] = this->m_tsdata[pid].queueheader[evenodd] = NULL;
        if (v->isLockable()) {
            v->lock.lock();
        }
        v->inGlobalQueue = false;
        return true;
    }


    // Queue has more than one element in it.
    v = this->m_tsdata[pid].queuetail[evenodd];
    if (v->isLockable()) {
        v->lock.lock();
    }
    assert(v->getPID() == pid);
    //boost::thread::id boost::this_thread::get_id();
    assert(v->gq_next == NULL);
    assert(v->gq_prev != NULL);

    this->m_tsdata[pid].queuetail[evenodd] = v->gq_prev;
    this->m_tsdata[pid].queuetail[evenodd]->gq_next = NULL;
    v->gq_prev = NULL;

    v->inGlobalQueue = false;
    return true;
}

void Graph::queueDecrementCounter(int pid) {
    boost::mutex::scoped_lock lock(m_tsdata[pid].queue_mutex);
    m_tsdata[pid].queue_counter--;
    if (m_tsdata[pid].queue_counter == 0) {
        assert(!m_done_flags[pid]);
        m_done_flags[pid]= true;
    }
}

bool Graph::isQueueEmpty() {
    for (size_t i = 0; i < num_procs; ++i) {
        if (!m_done_flags[i]){
            return false;
        }
    }
    return true;
}

void Graph::discharge(int pid) {
    this->ids[pid] = boost::this_thread::get_id();

    int localcounter = 0;
    size_t waitcounter = 0;
    int sleepcounter = 0;
    while (true) {
        Node *v = NULL;
        //while(q->front(pid,v)) {
        while (getFromQueue(v,pid)) {
            if (apply(v,pid) == WAIT) {
                waitcounter++;
            } else {
                localcounter++;
            }
            if (v->isLockable()) {
                v->lock.unlock();
            }
            // update only after certain number of operations.
            if (localcounter == G_COUNTER_UPDATE_FREQ) {
                m_dischargecounter += G_COUNTER_UPDATE_FREQ;
                localcounter = 0;
            }

            if (localcounter == 0 && (this->m_tsdata[pid].m_brelabel == DO_GLOBAL_RELABEL)) {
                // Start relabelling.
                this->global_relabel(pid);
                this->m_lsgr_mutex.lock();
                this->m_tsdata[pid].m_brelabel = !DO_GLOBAL_RELABEL;
                this->m_lsgr_mutex.unlock();
                m_lsgr_condition.notify_one();
            }
            //q->pop(pid);
            this->queueDecrementCounter(pid);
        }
        //boost::unique_lock<boost::mutex> conditional_lock(m_wait_mutexes[pid]);
        //if (!q->isempty()) {
        if (!this->isQueueEmpty()) {
            //usleep(1000);
            boost::this_thread::yield();
            if (localcounter > 0) {
                m_dischargecounter += localcounter;
                localcounter  = 0;
            }
            sleepcounter++;
            if ((this->m_tsdata[pid].m_brelabel == DO_GLOBAL_RELABEL)) {
                // Start relabelling.
                this->global_relabel(pid);
                this->m_lsgr_mutex.lock();
                this->m_tsdata[pid].m_brelabel = !DO_GLOBAL_RELABEL;
                this->m_lsgr_mutex.unlock();
                m_lsgr_condition.notify_one();
            }
            //m_wait_conditions[1].wait(conditional_lock);
        } else {
            break;
        }
        //conditional_lock.unlock();
    }
    boost::mutex::scoped_lock lock(m_lsgr_mutex);
    this->m_tsdata[pid].m_brelabel = !DO_GLOBAL_RELABEL;
    m_lsgr_condition.notify_one();
    std::cerr << "Wait counter for " << pid << " :"<< waitcounter << std::endl;
    std::cerr << "Sleep counter for " << pid << " :"<< sleepcounter << std::endl;

}


//void Graph::search_allocator(){
//    while (true) {
//        if (m_dischargecounter < GR_FACTOR * numNodes) {
//            sleep(GR_SLEEP_TIME);
//            if (q->isempty()) {
//                break;
//            }
//        }
//        // Increment wave number
//        m_wavenumber++;
//        m_currentLevel = 1;
//        //memset (m_done, 0, num_procs*sizeof(bool));

//        // Add all nodes in the residual graph which can send flow
//        // to this node.
//        boost::mutex::scoped_lock lock(m_lsgr_mutex);

//        while (!gr_q->isempty()) {
//            // Move contents to the separate queues
//            for (int i = 0; i < num_procs; ++i) {
//                deque<Task> tq = gr_q->getQueue(i);
//                if (!tq.empty()) {
//                    if (q->enqueueTasks(gr_q->getQueue(i),i)) {
//                        m_done[i] = false;
//                    }
//                }

//            }
//            while(!leveldone()) {
//                m_lsgr_condition.wait(lock);
//                if (q->isempty()) {
//                    return;
//                }
//            }
//            if (q->isempty()) {
//                std::cout << "Done GR \n";
//                return;
//            }
//            // Completed level.
//        }
//        // completed  GR
//    }
//    std::cout << "Done GR \n";
//}

void Graph::search_allocator() {
    m_wavenumber = 0;
    m_currentLevel = 0;
    while(true) {
        if (m_dischargecounter< GR_FACTOR*numNodes) {
            sleep(GR_SLEEP_TIME);
            //if (q->isempty()) {
            if (this->isQueueEmpty()) {
                break;
            }
        }

        boost::mutex::scoped_lock lock(m_lsgr_mutex);

        m_wavenumber++;

        std::cerr << " The max label is " << m_currentLevel << std::endl << std::flush;
        m_currentLevel = 1;

        for (size_t i = 0; i < num_procs; ++i){
            this->m_tsdata[i].m_brelabel = DO_GLOBAL_RELABEL;
        }

        while (!leveldone()) {
            m_lsgr_condition.wait(lock);
            if (this->isQueueEmpty()) {
                //if (q->isempty()){
                return;
            }
        }

        //SimpleConcurrentQueue *next_q = gr_q[(m_currentLevel+1)%2];
        //while (!next_q->isempty() && !q->isempty()) {
        //while (!next_q->isempty() && !this->isQueueEmpty()) {
        while (!isGlobalQueueEmpty((m_currentLevel+1)%2) && !this->isQueueEmpty()) {
            m_currentLevel++;
            for (size_t i = 0; i < num_procs; ++i){
                this->m_tsdata[i].m_brelabel = DO_GLOBAL_RELABEL;
            }

            //next_q = gr_q[(m_currentLevel+1)%2];
            while (!leveldone()) {
                m_lsgr_condition.wait(lock);
                if (this->isQueueEmpty()) {
                    //if (q->isempty()) {
                    return;
                }
            }
        }
        m_currentLevel = numNodes+1;

        for (size_t i = 0; i < num_procs; ++i){
            this->m_tsdata[i].m_brelabel = DO_GLOBAL_RELABEL;
        }

        while (!leveldone()) {
            m_lsgr_condition.wait(lock);
            //if (q->isempty()){
            if (this->isQueueEmpty()) {
                return;
            }
        }

        //next_q = gr_q[(m_currentLevel+1)%2];
        //while (!next_q->isempty() && !q->isempty()) {
        //while (!next_q->isempty() && !this->isQueueEmpty()) {
        while (!isGlobalQueueEmpty((m_currentLevel+1)%2) && !this->isQueueEmpty()) {
            m_currentLevel++;
            for (size_t i = 0; i < num_procs; ++i){
                this->m_tsdata[i].m_brelabel = DO_GLOBAL_RELABEL;
            }
            if (m_currentLevel == 10) {
                int i = 0;
                ++i;
            }
            //next_q = gr_q[(m_currentLevel+1)%2];
            while (!leveldone()) {
                m_lsgr_condition.wait(lock);
                //if (q->isempty()) {
                if (this->isQueueEmpty()) {
                    return;
                }
            }

        }

        m_dischargecounter = 0;
        //        for (int i = 0; i < numNodes; ++i) {
        //            if (nodes[i].wavenumber != m_wavenumber) {
        //                Node *katham = &nodes[i];
        //                std::cerr << "Wrong relabelling dude!";
        //            }
        //        }
    }
}

bool Graph::leveldone() {
    for(size_t i =0; i < num_procs; ++i) {
        if (this->m_tsdata[i].m_brelabel == DO_GLOBAL_RELABEL) {
            return false;
        }
    }
    return true;
}

Node *Graph::mark_shared(size_t i){
    assert(i < numNodes && "index out of bounds in Graph::mark_shared");
    return nodes[i].setShared(true);
}

Node *Graph::mark_lockable(size_t i) {
    assert(i < numNodes && "index out of bounds in Graph::mark_lockable");
    return nodes[i].setLockable(true);
}

Edge *Graph::create_new_edge(){
    assert((last_array_edge < edges + numEdges) && "Bounds exceeded at Graph::create_new_edge!");
    return last_array_edge++;
}

void Graph::add_edge(unsigned int i, unsigned int j, int cap, int rcap) {
    assert(i < numNodes && i >= 0);
    assert(j < numNodes && j >= 0);

    assert(cap >=0 );
    assert(rcap >=0);
    Edge *forward_edge = this->create_new_edge();
    Edge *backward_edge = this->create_new_edge();
    forward_edge->setMate(backward_edge);
    backward_edge->setMate(forward_edge);

    forward_edge->setNode(&nodes[j]);
    backward_edge->setNode(&nodes[i]);

    forward_edge->setCap(cap);
    backward_edge->setCap(rcap);

    nodes[i].add_edge(forward_edge);

    nodes[j].add_edge(backward_edge);

}

void Graph::add_edge1(unsigned int i, unsigned int j, int cap, bool createmate= false, size_t mateid=0) {
    assert (i < numNodes+2 && i >= 0 && "Index i out of bounds in Graph::add_edge");
    assert (j < numNodes+2 && j >= 0 && "Index j out of bounds in Graph::add_edge");
    assert (createmate || mateid < numEdges);

    Node *from = &this->m_tsdata[0].source;
    Node *to = &this->m_tsdata[0].sink;
    assert (!(i==numNodes && j == numNodes+1));
    if (i == numNodes) {
        //int pid = this->getPID(&nodes[j]);
        from = &this->m_tsdata[nodes[j].getPID()].source;

    }
    if (j == numNodes) {
        //int pid = this->getPID(&nodes[i]);
        to = &this->m_tsdata[nodes[i].getPID()].source;
    }

    if (i == numNodes+1 ) {
        // to has to be a normal node.
        //int pid = this->getPID(&nodes[j]);
        //from = &sink[nodes[j].getPID()];
        from = &this->m_tsdata[nodes[j].getPID()].sink;
    }
    if (j == numNodes+1) {
        // from has to a  normal node.
        //int pid = this->getPID(&nodes[i]);
        //to = &sink[nodes[i].getPID()];
        to = &this->m_tsdata[nodes[i].getPID()].sink;
    }

    if (i == numNodes ) {
        // to has to be a normal node.
        //int pid = this->getPID(&nodes[j]);
        //from = &source[nodes[j].getPID()];
        from = &this->m_tsdata[nodes[j].getPID()].source;
    }
    if (j == numNodes) {
        // from has to a  normal node.
        //int pid = this->getPID(&nodes[i]);
        //to = &source[nodes[i].getPID()];
        to = &this->m_tsdata[nodes[i].getPID()].source;
    }

    if (i < numNodes ) {
        from = &nodes[i];
    }
    if (j < numNodes) {
        to =  &nodes[j];
    }

    if (createmate) {
        Edge *edge= this->create_new_edge();
        edge->setCap(cap);
        edge->setNode(to);
        //  edge->setMate(sis);
        from->add_edge(edge);

        Edge *sis = this->create_new_edge();
        sis->setCap(0);
        sis->setNode(from);
        to->add_edge(sis);
        edge->setMate(sis);
        sis->setMate(edge);
    }
    else {
        Edge *edge = this->create_new_edge();
        edge->setCap(cap);
        edge->setNode(to);
        from->add_edge(edge);
        edge->setMate(&edges[mateid]);
    }
}

Graph *Graph::testGraph1() {
    unsigned int n,e;
    unsigned int S,T;
    char temp;
    string str;
    ifstream f("/home/shri/shared/Cuts/watever/Cuts/build/2dgraph.dimacs");

    int slices = 1;
    int rows = 27;
    int columns = 60;

    f >> temp >> str >> n >> e;
    f >> temp >> S >> temp >> temp >> T >> temp;

    cout << n << "  " << e << std::endl;
    Graph *g = new Graph(e,n-2, rows, columns, slices, DEF_NPROC );
    for (unsigned int i = 0;i < e; ++i) {

        int from = 0, to = 0;
        unsigned int cap;
        size_t mateid;
        size_t thisid;
        f >> temp >> from >> to >> cap >> mateid >> thisid;
        //assert(thisid == i+1);

        g->add_edge1(from-1, to-1, cap, false, mateid-1);
    }

    g->isValid();
    return g;
}

Graph *Graph::testGraph2() {
    unsigned int n,e;
    unsigned int S,T;
    char temp;
    string str;
    ifstream f("/home/shri/shared/Cuts/watever/Cuts/build/3dgraph.dimacs");
    int slices = 10;
    int rows = 27;
    int columns = 60;

    f >> temp >> str >> n >> e;
    f >> temp >> S >> temp >> temp >> T >> temp;

    cout << n << "  " << e << std::endl;
    Graph *g = new Graph(e,n-2, rows, columns, slices, DEF_NPROC );
    for (unsigned int i = 0;i < e; ++i) {

        int from = 0, to = 0;
        unsigned int cap;
        size_t mateid;
        size_t thisid;
        f >> temp >> from >> to >> cap >> mateid >> thisid;
        //assert(thisid == i+1);

        g->add_edge1(from-1, to-1, cap, false, mateid-1);
    }

    g->isValid();
    return g;
}

void Graph::isValid() {
    for(size_t i = 0; i < numEdges; ++i) {
        Edge *edge = &edges[i];
        assert (edge->getMate()->getMate()->getNode() == edge->getNode());
    }
}

Graph *Graph::testGraph() {
    int k = 2+1;
    int rows = 5;
    int cols = 2*k+1;

    int nodeCount = cols * rows;
    int edgeCount = 2*nodeCount + 2*rows;
    Graph *g = new Graph(edgeCount*2, nodeCount, rows, cols, 1,2);
    int sourceid = nodeCount;
    int sinkid = sourceid+1;
    SizingOracle so(rows, cols, 1);
    //    g->sink.c = 36;
    //    g->source.c = 35;
    // Add a edge from source to all nodes in first column.
    for (int i = 0; i < rows; ++i) {
        // Nodes are indexed in a column major order similar to that in Matlab.
        // This is done so that the nodes that are common to a processor appear
        // in contigous memory locations.
        size_t index = so.sub2ind(i, 0, 1);
        g->add_edge1(sourceid, index, 100);
        index = so.sub2ind(i, cols-1, 1);
        g->add_edge1(index, sinkid, 100);
    }

    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; j++) {
            // For every node add edge to right and bottom.
            size_t index = so.sub2ind(i,j,1);
            if (i < rows -1) {
                // bottom possible
                size_t bi = so.sub2ind(i+1, j, 1);
                g->add_edge1(index, bi, 100);
            }
            if (j < cols -1) {
                // right possible
                size_t ri = so.sub2ind(i, j+1, 1);
                int cap = 100;
                if (j+1 == k) {
                    cap = 1;
                }
                g->add_edge1(index, ri, cap);
            }
        }
    }
    return g;
}

void Graph::initialize_preflow() {
    // For all the edges coming out of the sink, push the m_cap amount of flow to it.
    for(size_t i = 0; i < num_procs; ++i) {
        //Edge *edge_list = source[i].edge_list;
        Edge *edge_list = m_tsdata[i].source.edge_list;
        while(edge_list != NULL){
            Node *v = edge_list->getNode();
            v->excess += edge_list->getCap();
            edge_list->increaseFlow(edge_list->getCap());
            edge_list = edge_list->next;
            if (v->excess != 0 && !v->source && !v->sink){
                addToQueue(v);
            }
        }
    }

    for (size_t i = 0; i < numNodes; ++i){
        nodes[i].current_edge = nodes[i].edge_list;
    }

    for (size_t i = 0;  i < num_procs;++i) {
        //source[i].current_edge = source[i].edge_list;
        m_tsdata[i].source.current_edge = m_tsdata[i].source.edge_list;
        //sink[i].current_edge = sink[i].edge_list;
        m_tsdata[i].sink.current_edge = m_tsdata[i].sink.edge_list;
    }
}

//void Graph::dump_dimacs() {
//    bool myway = true;
//    ofstream f("2dgraph.dimacs");
//    unsigned int ne = numEdges;

//    for (int i = 0; i < numEdges; ++i) {
//        edges[i].id = i+1;
//    }
//    int sid = numNodes + 1;
//    int tid = numNodes + 2;

//    f << "p max "<< numNodes+2 <<" " << ne << std::endl;
//    f << "n " << sid <<  " s" << std::endl;
//    f << "n " << tid <<  " t" << std::endl;

//    for (size_t i = 0; i < numNodes; ++i) {
//        Edge *elst = nodes[i].edge_list;
//        while (elst) {
//            if (elst->getNode()->source) {
//                f << "a " << i+1 << " " << sid << " " << elst->getCap() <<  "  " << elst->getMate()->id << std::endl;
//            } else if (elst->getNode()->sink) {
//                f << "a " << i+1 << " " << tid << " " << elst->getCap() <<  "  " << elst->getMate()->id << std::endl;
//            }
//            else {
//                f << "a " << i+1 << " " << (elst->getNode()-nodes) + 1 << " " << elst->getCap() <<  "  " << elst->getMate()->id << std::endl;
//            }
//            elst = elst->next;
//        }
//    }
//    Edge *elst = sink.edge_list;
//    while (elst) {
//        if (elst->getNode()->source) {
//            f << "a " << tid << " " << sid << " " << elst->getCap() <<  "  " << elst->getMate()->id << std::endl;
//        } else if (elst->getNode()->sink) {
//            f << "a " << tid << " " << tid << " " << elst->getCap() <<  "  " << elst->getMate()->id <<  std::endl;
//        }
//        else {
//            f << "a " << tid << " " << (elst->getNode()-nodes) + 1 << " " << elst->getCap() <<  "  " << elst->getMate()->id << std::endl;
//        }
//        elst = elst->next;
//    }

//    elst = source.edge_list;
//    while (elst) {
//        if (elst->getNode()->source) {
//            f << "a " << sid << " " << sid << " " << elst->getCap() <<  "  " << elst->getMate()->id << std::endl;
//        } else if (elst->getNode()->sink) {
//            f << "a " << sid << " " << tid << " " << elst->getCap() <<  "  " << elst->getMate()->id << std::endl;
//        }
//        else {
//            f << "a " << sid << " " << (elst->getNode()-nodes) + 1 << " " << elst->getCap() <<  "  " << elst->getMate()->id << std::endl;
//        }
//        elst = elst->next;
//    }

//}
