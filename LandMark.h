#ifndef LANDMARK_H
#define LANDMARK_H

using namespace std;

class point
{
public:
    int data[3];
	
private:
	void init()
	{
		data[0] = 0;
		data[1] = 0;
        data[2] = 0;
	}

public:
    point(){
		init();
	}

    point(int x, int y, int z)
	{
		data[0] = x;
		data[1] = y;
        data[2] = z;
	}

	point(const point& A)
	{
		data[0] = A[0];
		data[1] = A[1];
        data[2] = A[2];
	}

    int& operator[](int pos)
	{
//        if(pos < 0 || pos > 1)
//        {
//          cout << "ERROR: access violation. Index has to be between 0 or 1." << endl;
//        }
	  return data[pos];
	}

    int operator[](int pos) const
    {
//        if(pos < 0 || pos > 1)
//        {
//            cout << "ERROR: access violation. Index has to be between 0 or 1." << endl;
//        }
    return data[pos];
  } 
};

#endif // LandMark_H
