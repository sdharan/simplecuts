#ifndef DS_H
#define DS_H

#include <iostream>
#include <boost/thread/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/atomic.hpp>
#include <assert.h>
#include <queue>
#include <fstream>
#include <time.h>
//include "concurrentqueue.h"
#include "defs.h"

using namespace std;

enum status {SUCCESS, WAIT, END};
enum ranktype {WHITE, GREY, BLACK};

// Spinlocks may actually be better than mutex for our implementation.
// TODO analyze timings
// http://stackoverflow.com/questions/19742993/implementing-a-spinlock-in-boost-example-needed
class SpinLock
{
    boost::atomic_flag flag; // it differs from std::atomic_flag a bit -
                             // does not require ATOMIC_FLAG_INIT
public:
    void lock()
    {
        while( flag.test_and_set(boost::memory_order_acquire) )
            ;
    }
    bool try_lock()
    {
        return !flag.test_and_set(boost::memory_order_acquire);
    }
    void unlock()
    {
        flag.clear(boost::memory_order_release);
    }
};

struct SizingOracle {
    int rows,columns,slices;
    SizingOracle() {}
    SizingOracle(int rows,int columns, int slices): rows(rows), columns(columns), slices(slices){

    }
    size_t sub2ind(int r, int c, int s) {
        // Works only for 2d now
        assert (columns >0);
        int psize=  rows*columns;
        //return s*(rows*columns) *r*columns+ c;
        return s*psize + r*columns + c;
    }
};

class Node;
class Edge {
    Node *to; // pointer to node
    Edge *mate; // Pointer to its mate( comes before or after this in the global array of edges
    int rcap; // residual capacity not strictly needed, but helpful when debugging
public:
    Edge *next;
    //size_t id; // remove on release

    Edge() :  to(NULL), mate(NULL), rcap(0){}

    inline int getCap() {
        return rcap;
    }

    inline Edge *getMate() {
        return mate;
    }

    inline void setCap(int cap)
    {
        this->rcap = cap;
    }

    inline void setMate(Edge *edge)
    {
        this->mate = edge;
    }

    inline void setNode(Node *node) {
        this->to = node;
    }

    inline void increaseFlow(size_t del)
    {
        this->rcap -= del;
        this->mate->rcap += del;
    }

    inline Node *getNode() {
        return to;
    }
};


class Node { // todo hide details
    int pid;
    char _padding_[24];
public:
    ranktype rank;
    bool st:4;
    // LockFree access
    bool sink:1;
    bool source:1;

    // Locked Access
    Edge *edge_list;
    Edge *current_edge;
    Edge *relabel_scan_list;

    unsigned long int label;
    int excess; // denotes the difference between the incoming flow and the outgoing flow at this vertex
    boost::atomic<bool> inQueue;
    boost::atomic<bool> inGlobalQueue;
    // shared vertices are the vertices which are between 2 segments.
    // lockable vertices are the vertices that have a shared vertex in its immediate neighborhood.
    // If a node is lockable, any update to the locked access member variables need to be done with lock.
    bool lockable:1;
    bool shared:1;
    size_t wavenumber;
    //int r,c,s;

public:
    Node *q_next; // why public ?
    Node *q_prev;
    SpinLock lock; // TODO refactor locking mechanism to a inline function


    Node();
    void add_edge(Edge *e);

    Node *setLockable(bool val);
    Node *setShared(bool val);

    inline void setPID(int pid) {
        this->pid = pid;
    }

    inline int getPID() {
        return this->pid;
    }

    bool isSink();
    bool isSource();
    bool isLockable();
    bool isShared();

    // For global relabelling queues

    Node *gq_next;
    Node *gq_prev;
};

// the tasks were created to test the possibility of having the
// global relabelling done by the individual processors themselves,
// instead of a separate thread to do it.
enum TaskType { PUSH, GRELABEL, GLABELEND};
struct Task {
    TaskType type;
    Node *node;
};


class SimpleConcurrentQueue
{
    std::vector<std::queue<Node*> > _queue;
    boost::mutex *_mutex;
    boost::shared_mutex _count_mutex;
    int n;
    bool trackInQueue:1;
public:
    SimpleConcurrentQueue(int num, bool trackInQueue ):n(num), trackInQueue(trackInQueue) {
        _queue.resize(n);
        _mutex = new boost::mutex[n];
    }

    bool front(int pid, Node *&t) {
        boost::shared_lock<boost::shared_mutex> lock(_count_mutex);
        _mutex[pid].lock();
        if (_queue[pid].empty()) {
            _mutex[pid].unlock();
            return false;

        } else {
            t = _queue[pid].front();

            if (trackInQueue) {
                t->inQueue = false; // inqueue is atomic. locking is not actually required here. Right ?
            }
            //_queue[pid].pop();
            _mutex[pid].unlock();
            return true;
        }

    }

    void pop(int pid) {
        boost::shared_lock<boost::shared_mutex> lock(_count_mutex);
        _mutex[pid].lock();
        if (_queue[pid].empty()) {
            assert(false);
            _mutex[pid].unlock();
        } else {
            _queue[pid].pop();
            _mutex[pid].unlock();
            //return true;
        }
    }

    void push(Node* val, int pid) {
        assert(pid < n);
        boost::shared_lock<boost::shared_mutex> lock(_count_mutex);
       _mutex[pid].lock();
       _queue[pid].push(val);
       if (trackInQueue) {
           assert(!val->inQueue);
           val->inQueue = true;
       }
       _mutex[pid].unlock();
    }


    bool isempty() {
        boost::unique_lock< boost::shared_mutex > lock(_count_mutex);
        for (int i = 0; i < n; i++) {
            if (!_queue[i].empty()){
                return false;
            }
        }
        return true;
    }

    std::queue<Node*> getQueue( int pid) {
        boost::shared_lock<boost::shared_mutex> lock(_count_mutex);
        boost::mutex::scoped_lock _queue_lock(_mutex[pid]);
        return _queue[pid];
    }

    void emptyQueue(int pid) {
        boost::shared_lock<boost::shared_mutex> lock(_count_mutex);
        boost::mutex::scoped_lock _queue_lock(_mutex[pid]);
        std::queue<Node*> empty;
        std::swap( _queue[pid], empty );
    }

};


// size 128, c++ seems to align it well.
struct ThreadStructures {

    ThreadStructures() :
        m_brelabel(false), queue_counter(0), header(NULL), tail(NULL) {
        queueheader[0] = queueheader[1] = queuetail[0] = queuetail[1] = NULL;
        global_queue_counter[0] = global_queue_counter[1] = 0;
        sink.source = false;
        sink.sink = true;
        source.source = true;
        source.sink = false;
        sink.setLockable(true);
        source.setLockable(true);
    }

    bool m_brelabel; // 1
    boost::mutex queue_mutex; // 40
    boost::atomic<size_t> queue_counter;// 8
    Node *header, *tail; //16
    Node *queueheader[2], *queuetail[2]; //32
    boost::mutex gobal_queue_mutex; // 40
    boost::atomic<size_t> global_queue_counter[2];
    //char _padding_[32];
    Node sink, source;
    char _padding_[96];
};

class Graph {
    Node* nodes;
    Edge *edges;
    Edge *last_array_edge; // used by create new edge

    size_t numEdges;
    size_t numNodes;
    size_t num_procs;
    size_t rows, columns, slices;
    size_t cols_per_segment;
    size_t m_wavenumber;
    size_t m_currentLevel;

    //Node *sink,*source;

    //SimpleConcurrentQueue *q;
    //SimpleConcurrentQueue *gr_q[2];

    SizingOracle so;

    boost::atomic<size_t> m_dischargecounter;

    boost::condition_variable m_lsgr_condition;
    boost::mutex m_lsgr_mutex;
    //bool *m_brelabel;


    // TODO replace sleep conditions with a condition variable
    //    boost::condition_variable *m_wait_conditions;
    //    boost::mutex *m_wait_mutexes;

    // DS for Gap relabelling
    boost::mutex *m_label_counter_mutex;
    std::vector<int> label_counter;
    bool GapFound:1;
    size_t GapLevel;
    boost::mutex m_gap_mutex;

    boost::atomic<bool> *m_done_flags;
    // Queue implementation using just node pointers
    //    Node **headers;
    //    Node **tails;
    //    boost::mutex *queue_mutex;
    //    boost::atomic<size_t> *queue_counters;

    // Queue implementation for global relabeling
    //Node **global_headers;
    //Node **global_tails;
    //boost::mutex *queue_mutex;
    ThreadStructures *m_tsdata;

public:
    Graph (size_t num_edges, size_t num_nodes, size_t rows, size_t cols, size_t slices, size_t num_procs = 0);
    ~Graph() {
        delete [] nodes;
        delete [] edges;
        delete [] m_done_flags;
        delete[] m_tsdata;
    }

    inline size_t getRows() {
        return rows;
    }
    inline size_t getCols() {
        return columns;
    }
    inline size_t getSlices() {
        return slices;
    }
    inline int what_segment(unsigned int id) {
        return nodes[id].st ? 1: 0;
    }

    void add_tweights(unsigned int id, float cap_source, float cap_sink);

    //size_t getPID(Node *u);
    Node* mark_shared(size_t i);
    Node* mark_lockable(size_t i);
    Edge* create_new_edge();
    void add_edge(unsigned int i, unsigned int j, int cap, int rcap = true);
    size_t getFlowValue();

    void initialize_preflow();
    size_t maxflow();
    void discharge(int pid);
    status apply(Node *u, int pid);
    void relabel(Node *v, int pid);
    void addToQueue(Node *v, bool getLock = true, int pid = -1);
    void search_allocator();
    void global_relabel(size_t pid);

    bool leveldone();
    void isValid();

    void updateLabelCounter(size_t oldValue, size_t newValue);

    void add_edge1(unsigned int i, unsigned int j, int cap, bool createmate, size_t mateid);
    static Graph* testGraph();
    static Graph *testGraph1();
    static Graph *testGraph2();
    void dump_segmentation(string filename);
    bool getFromQueue(Node *&v, int pid);
    void queueDecrementCounter(int pid);
    bool isQueueEmpty();
    void addToGlobalQueue(Node *v, int pid, int evenodd);
    bool getFromGlobalQueue(Node *&v, int pid, int evenodd);
    bool isGlobalQueueEmpty(int evenodd);

    // Debug info
    boost::thread::id *ids;
};


//class ConcurrentBucketQueue {
//    std::vector<std::vector< Node*> > _queuehead;
//    std::vector<std::vector< Node*> > _queuetail;
//    std::vector<int> _lastlabels; // Stores the highest label for the nprocs
//    std::vector<int> _lastprocessed; // Stores the value that was last serviced for the nprocs
//    std::vector<size_t> _queuesize; // Stores the queue sizes for the nprocs

//    boost::mutex *_mutex;
//    boost::shared_mutex _count_mutex;
//    int nProcs, nlabels;
//    bool trackInQueue:1;
//    bool gapFlag:1;

//public:
//    ConcurrentBucketQueue(int num, bool trackInQueue , int numNodes): nProcs(num), trackInQueue(trackInQueue), nlabels(numNodes) {
//        _queuehead.resize(nProcs);
//        _queuetail.resize(nProcs);
//        for (int i = 0; i < nProcs; i++) {
//            _queuehead[i].resize(nlabels);
//            _queuetail[i].resize(nlabels);
//        }
//        _lastlabels.resize(nProcs,0);
//        _mutex = new boost::mutex[nProcs];
//        _lastprocessed.resize(nProcs, -1);
//    }

//    bool front(int pid, Node *&t) {
//        boost::shared_lock<boost::shared_mutex> lock(_count_mutex);
//        boost::mutex::scoped_lock _queue_lock(_mutex[pid]);

//        // Get the highest label queue from the queue
//        if (_queuetail[pid][_lastlabels[pid]]) {
//            t = _queuetail[pid][_lastlabels[pid]];
//            _lastprocessed[pid] = _lastlabels[pid];
//            if (trackInQueue) {
//                t->inQueue = false; // inqueue is atomic. locking is not actually required here. Right ?
//            }
//            return true;
//        } else {
//            return false;
//        }
//    }

//    void pop(int pid) {
//        boost::shared_lock<boost::shared_mutex> lock(_count_mutex);
//        boost::mutex::scoped_lock _queue_lock(_mutex[pid]);
//        // Remove the node from the list.
//        if (_queuetail[pid][_lastprocessed[pid]]->q_prev){
//            _queuetail[pid][_lastprocessed[pid]] = _queuetail[pid][_lastprocessed[pid]]->q_prev;
//            _queuetail[pid][_lastprocessed[pid]]->q_next->q_prev =  NULL;
//            _queuetail[pid][_lastprocessed[pid]]->q_next =  NULL;
//        } else {
//            // End of bucket. Make it null and set lastlabels to the next highest value.
//            _queuetail[pid][_lastprocessed[pid]]->q_next = _queuetail[pid][_lastprocessed[pid]]->q_prev = NULL;
//            _queuetail[pid][_lastprocessed[pid]] = _queuehead[pid][_lastprocessed[pid]] = NULL;

//            while (--_lastlabels[pid] > -1) {
//                if (_queuetail[pid][_lastlabels[pid]]) {
//                    break;
//                }
//            }
//        }
//        _queuesize[pid]--;
//    }

//    void push(Node* val, int pid) {
//        assert(pid < nProcs);
//        boost::shared_lock<boost::shared_mutex> lock(_count_mutex);
//       _mutex[pid].lock();
//       _queuesize[pid]++;

//       if (_queuehead[pid][val->label] == NULL) {
//           // First element in queue.
//           _queuehead[pid][val->label] = val;
//           _queuetail[pid][val->label] = val;
//           val->q_next = val->q_prev = NULL;
//           if (_lastlabels[pid] < val->label) {
//               _lastlabels[pid] = val->label;
//           }
//       }
//       else {
//           val->q_next = _queuehead[pid][val->label];
//           _queuehead[pid][val->label]->q_prev = val;
//           _queuehead[pid][val->label] = val;
//           val->q_prev = NULL;
//       }

//       if (trackInQueue) {
//           assert(!val->inQueue);
//           val->inQueue = true;
//       }
//       if (_lastlabels[pid] < val->label) {
//           _lastlabels[pid]   = val->label;
//       }
//       _mutex[pid].unlock();
//    }


//    bool isempty() {
//        boost::unique_lock< boost::shared_mutex > lock(_count_mutex);
//        for (int i = 0; i < nProcs; i++) {
//            if (!_queuehead[i].empty()){
//                return false;
//            }
//        }
//        return true;
//    }

//    std::queue<Node*> getQueue( int pid) {
//        boost::shared_lock<boost::shared_mutex> lock(_count_mutex);
//        boost::mutex::scoped_lock _queue_lock(_mutex[pid]);
//        return _queuehead[pid];
//    }
//    void emptyQueue(int pid) {
//        boost::shared_lock<boost::shared_mutex> lock(_count_mutex);
//        boost::mutex::scoped_lock _queue_lock(_mutex[pid]);
//        std::queue<Node*> empty;
//        std::swap( _queuehead[pid], empty );
//    }

//};
#endif // DS_H
