#ifndef HORIZONSEG2_H
#define HORIZONSEG2_H

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <string>
#include <sstream>
#include <cmath>
#include "ds.h"
#include "LandMark.h"
#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkImageRegionIteratorWithIndex.h"
#include "itkImageRegionConstIteratorWithIndex.h"
#include "itkShrinkImageFilter.h"
#include "itkRecursiveGaussianImageFilter.h"
#include "time.h"

using namespace std;
// TODO set to MAX_INT or maybe something a little less than MAX_INT
#define INFVAL 1E7
#define EPS 1E-5
#define BETA 2
#define SCALAR 1000
#define ARCWINDOW 10 // window size for arcs in adjacent columns

typedef vector< int > vec1i;
typedef vector< vec1i > vec2i;
typedef vector< vec2i > vec3i;
typedef vector< float> vec1f;
typedef vector< vec1f > vec2f;
typedef vector< vec2f > vec3f;
typedef Graph GraphType;
// Image setup types
typedef itk::Image< float,3 > ImageType;
typedef itk::ImageFileReader<ImageType> ReaderType;
typedef itk::ImageRegionIteratorWithIndex< ImageType > IteratorType;
typedef itk::ImageRegionConstIteratorWithIndex< ImageType > ConstIteratorType;
typedef itk::ImageFileWriter<ImageType> WriterType;
typedef itk::Image< int, 3 > ImageTypeidx;

class HorizonSegmenter
{
  public:
    HorizonSegmenter(); // default constructor
    HorizonSegmenter(float ***iparray); // parametric constructor
    HorizonSegmenter(string ipfile, int smpl_fctr, unsigned int& d0, unsigned int& d1, unsigned int& d2, bool doParallel = false);
    HorizonSegmenter(ImageType::Pointer image, int smpl_fctr, unsigned int& d0, unsigned int& d1, unsigned int& d2);
    float*** extractmatrixfromfile(string inputfile, int& m_R, int& m_C, int& m_S, int& m_smpl_fctr, unsigned int& m_D0, unsigned int& m_D1, unsigned int& m_D2);
    float*** extractmatrixfromimage(ImageType::Pointer ipimage, int& m_R, int& m_C, int& m_S, int& m_smpl_fctr, unsigned int& m_D0, unsigned int& m_D1, unsigned int& m_D2);
    vec3f extract_iparray(float ***inputarray, int& m_R, int& m_C, int& m_S);

    void AddLandmark(point p);
    void SetSmoothnessConstraint (int smoothness);
    void SetSwitchToPassThroughLandmark(int lmswitch);
    void SetGraphCutMethod(int gcmethod); // 1 - gc, 0 - dp
    void SetProfileLength(int sticklength);
    void SetOutputImageName(string opimage);
    void SetNarrowBandFactor(int narrowband);

////    Solver related member functions
    void SortLandmarks();
    void Memoizelandmarks();
    void Solver();
    void ExtractProfiles(float*** &modelprofiles,float*** &idxprofiles);
    float BiLinearInterpolator(point p1, point p2, float p1val, float p2val, int p3); // internal function of ExtractProfiles to compute inter-intensities
    void left_columns(vec3f& lm_stkprfls, vec3f& lm_idxprfls, int& p, float*** &modelprofiles, float*** &idxprofiles); // left columns computation
    void right_columns(vec3f& lm_stkprfls, vec3f& lm_idxprfl, int& p, float*** &modelprofiles, float*** &idxprofiles); // in-between columns computation
    void inbetween_columns(vec3f& lm_stkprfls, vec3f& lm_idxprfl, int& s, float*** &modelprofiles, float*** &idxprofiles); // in-between columns computation
    void front_slices(float*** &modelprofiles, float*** &idxprofiles); // front slice computation
    void back_slices(float*** &modelprofiles, float*** &idxprofiles); // back slice computation
    void inbetween_slices(float*** &modelprofiles, float*** &idxprofiles); // in between slice computation
    void SmallImage(float*** &idxprofiles);
    void CostCompute(float*** &modelprofiles, float*** &costs);
//    void SmallImage(float*** &idxprofiles, float*** &smallarray);
//    void CostCompute(float*** &modelprofiles, float*** &smallarray, float*** &costs);
    void WeightCompute(float*** &costs);
    void DynamicProg(float*** &costs);
    void BuildGraph();
    void edge_cost(vec1i& delta_ij);
    void graph_construct_stage0(int& current_sl);
    void graph_construct_stage1();
    void graph_partition_stage0(int& current_sl);
    void graph_partition_stage1();
    void HorizonOutput();

    vec3i GetRegion();
    vec2f GetBoundary();
    void WriteOutputImage();

    // member variables
    bool m_stage1; // flag to work on either stage1 or stage2
    vector<point> m_landmark;
    vector< vector<point> > m_landmarks_per_sl;
    vec1f m_lmspersl;
    vec1i m_lm_slices;
    int m_stage1_sl;
    vec3f m_inputarray;
    int m_origR, m_origC, m_origS;
    int m_R, m_C, m_S;
    unsigned int m_D0, m_D1, m_D2;
    int m_modiR;
    int m_smpl_fctr; // factor to resample the image
    int m_smooth;
    int m_lmswitch;
    int m_gcmethod;
    int m_sticklength;
    int m_narrowband;
    float*** m_Extidxprofiles;
    vec3f  m_weights;
    vec3i m_DP;
    // int* m_basearcs;
    GraphType *m_g;
    vec3i m_gc;
    vec2i m_newlm_rows;
    vec3i m_segmentregion;
    vec2f m_segmentboundary;
    string m_opimagename;


};

ImageType::Pointer image_resample(ImageType::Pointer ip_image, int& smplfctr, unsigned int& d2);
float LinearInterpolator(int &x0, int &x1, int &y0, int &y1, int &x); // Linear interpolation with ints
float LinearInterpolator(int &x0, int &x1, float &y0, float &y1, int &x); // Linear interpolation with ints/floats
void PointProcess(int &pt, int &RowMax);
bool compareD0D2(point a, point b); // comparision function w.r.to dimension 0, then dimension 2
void allocate_memory(float*** &vec3d, int &slc, int& row, int &col);
void destruct_memory(float*** &vec3d, int &slc, int& row, int &col);
int stage_param(bool stage, int &stage1_param, int& stage2_param);
void bdry_row_set(int &curr_row, int &row_limit);
vec1f gauss1d(int& length); // gaussian 1D filter
void scale_profile(vec1f& img_prfl, vec1f& gauss_prfl, int& length);
float dotprod(vector<float> &vec1, vector<float> &vec2, int &length); // dot product of two vectors
vec3f flip_matrix(bool stage, float*** &vec3D, int& d0size, int& d1size, int& d2size);
float DELTASL(unsigned int f, int alpha, int beta); // VCEnet connections for same level
float DELTALB(unsigned int f, int alpha, int beta); // VCEnet connections for level below it
void CreateImage(ImageType::Pointer image, int& NumSlices, int& NumRows, int& NumCols,
                 unsigned int& d0, unsigned int& d1, unsigned int& d2);

#endif // HorizonSeg2_H

