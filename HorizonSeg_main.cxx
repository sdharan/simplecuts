// This script is for building an API for exxonmobil project with respect to surface segmentation using graph-cuts
#include "HorizonSeg2.h"
#include "LandMark.h"
#include <string>
using namespace std;

enum GC_technique{Vnet,VCEnet};

int main(int argc, char* argv[]){
    if(argc < 9){
        std::cerr << "Usage: " << std::endl;
        std:: cerr << argv[0] << " InputImage" << " Smoothness" << " Method" <<
			" LandmarkSwitch" << " WindowSize" << " LayersPerPixel" << " OutputImage" <<
            "  bounding box factor " ;
        return EXIT_FAILURE;
    }
	// Default input parameter settings: input_image 100 1 1 11 1 output_image 3

    uint D0 = 1;
    uint D1 = 2;
    uint D2 = 0;

    std::cerr << "doparallel is" << argv[9] << std::endl;
    HorizonSegmenter HS2(string(argv[1]), atoi(argv[6]), D0, D1, D2);


	//    landmarks for Gradientimage3d_2.mhd
//    int LMarray[14][3] = { {0, 24, 78}, {0, 32, 211}, {5, 27, 89}, {5, 32, 175}, {8,73,940}, {8,65,796}, {8,58,670},
//                           {34,76,709},{34,64,529},{34,34,85},
//                           {45,37,83}, {45,54,321}, {45,53,321}, {45,87, 768}}; // 2d array initialization

//        int LMarray[10][3] = { {48,6,234}, {87, 6, 234}, {109,6, 219}, {168,6,213}, {228,6,210}, {15,17,29}, {66,16,235},
//                               {100,16,230},{135,16,219},{213,16,212}
//                               /*{7,14,221}, {68,14,232}, {150,14,215}, {238,14, 203}*/}; // 2d array initialization

//    int LMarray[13][3] = { {12,6,224}, {12, 18, 224}, {36,6,231}, {65,8,234}, {65,19,235}, {99,3,224}, {99,23,236},
//                                   {133,4,217},{133,22,220},{201,3, 214}, {201, 21, 212}, {235, 4, 208}, {235, 15,207}
//                                   /*{7,14,221}, {68,14,232}, {150,14,215}, {238,14, 203}*/}; // 2d array initialization


    //int LMarray[6][3] = {{56 ,6 ,36}, {59 ,22 ,37}, {185 ,6 ,16}, {201 ,22 ,16}, {    240 ,6 ,6} , {    241 ,22 ,4}};
    //int LMarray[6][3] = { {    7  ,13  ,22}, {    35  ,7  ,27},{ 77  ,7  ,34}, {79  ,0  ,34}, { 86  ,5  ,36}, {    178  ,7  ,15}};

    // int LMarray[10][3] = {{4 ,16 ,153},    {14 ,18 ,150},    {36 ,32 ,155},    {56 ,16 ,154},    {95 ,32 ,160},    {99 ,18 ,157},    {106 ,16 ,155},    {156 ,16 ,146},    {163 ,32 ,149},    {227 ,18 ,145}};

    //int LMarray[5][3] = { {8,11,28}, {8,22,42}, {8,14,32},
//                          {3,22,37},{3,10,22} }; // 2d array initialization

    // Resampled crop liang
    int LMarray[5][3] = { {6,8,9}, {28,8,13}, {52,8,8},
                              {20,1,12},{68,1,5} }; // 2d array initialization

    uint NoLandmarks = sizeof(LMarray)/sizeof(LMarray[0]); // sizeof(LMarray)/sizeof(*LMarray)
    std::cout << "Number of landmarks: " << NoLandmarks << "\n";
    for(uint m = 0; m < NoLandmarks; m++){
        point p;
        p[0] = LMarray[m][0];
        p[1] = LMarray[m][1];
        p[2] = LMarray[m][2];
        HS2.AddLandmark(p);
    }

    HS2.SetSmoothnessConstraint(atoi(argv[2])); // setting smoothness constraint
    HS2.SetGraphCutMethod(atoi(argv[3])); // setting Graph-cut method
    HS2.SetSwitchToPassThroughLandmark(atoi(argv[4])); // radio switch to pass through landmark
    HS2.SetProfileLength(atoi(argv[5])); // setting window size
    HS2.SetOutputImageName(argv[7]); // setting output image name
    HS2.SetNarrowBandFactor(atoi(argv[8])); // bounding box factor

	//    // calling wrapper
    HS2.Solver();
    HS2.WriteOutputImage();
	//    vec3i SegRegion = HS2.GetRegion();
	//    vec2f SegHorizon = HS2.GetBoundary();
}



